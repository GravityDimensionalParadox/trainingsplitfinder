#include "../src/conditions.cpp"

bool atleastXdaysApartTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        bool test1 = atleastXdaysApart ('L', 'C', 1, testTrainingSchedule);
        bool test2 = atleastXdaysApart ('S', 'C', 1, testTrainingSchedule);

        bool test3 = atleastXdaysApart ('S', 'A', 1, testTrainingSchedule);
        bool test4 = atleastXdaysApart ('B', 'N', 1, testTrainingSchedule);
        bool test5 = atleastXdaysApart ('L', 'C', 3, testTrainingSchedule);

        if
        (
                test1 == true &&
                test2 == true &&
                test3 == false &&
                test4 == false &&
                test5 == false
        )
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool notOnDayXTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        bool test1 = notOnDayX ('L', 3, testTrainingSchedule, 0);
        bool test2 = notOnDayX ('S', 2, testTrainingSchedule, 0);
        bool test3 = notOnDayX ('A', 1, testTrainingSchedule, 1);
        bool test4 = notOnDayX ('S', 1, testTrainingSchedule, 1);
        bool test5 = notOnDayX ('B', 2, testTrainingSchedule, 5);

        cout << test1 << '\n';
        cout << test2 << '\n';
        cout << test3 << '\n';
        cout << test4 << '\n';
        cout << test5 << '\n';

        if
        (
                test1 == true &&
                test2 == false &&
                test3 == true &&
                test4 == false &&
                test5 == false
        )
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool findConditionsTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector< vector<string> > testConditionList = {{"A", "L", "A", "1"}, {"A", "C", "L", "2"}, {"N", "C", "C", "1"}};
        vector< vector<string> > testConditionList2 = {{"A", "R", "B", "2"}};
        vector< vector<string> > testConditionList3 = {{"N", "S", "S", "2"}};
        bool test1 = findConditions (testTrainingSchedule, testConditionList, 0);
        bool test2 = findConditions (testTrainingSchedule, testConditionList2, 0);
        bool test3 = findConditions (testTrainingSchedule, testConditionList3, 0);

        if
        (
                test1 == true &&
                test2 == false &&
                test3 == false
        )
        {
                return true;
        }
        else
        {
                return false;
        }
}

int main ()
{
        //atleastXdaysApartTest (); PASSED
        //notOnDayXTest (); PASSED
        //findConditionsTest (); PASSED
}
