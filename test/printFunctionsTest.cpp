#include "../src/printFunctions.cpp"

void printWeekArrayTest ()
{
        vector<char> testTrainingDaysLetters = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        printWeekArray (testTrainingDaysLetters);
}

void printByLetterTest ()
{
        vector<char> testTrainingDaysLetters = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        vector<string> testTrainingDaysNames = {"Arms", "Rest", "Chest", "Back", "Shoulders", "Legs", "Neck"};
        cout << printByLetter ('B', testTrainingDaysLetters, testTrainingDaysNames) << '\n';
}

void printWeekArrayNiceTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};

        vector<char> testTrainingDaysLetters = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        vector<string> testTrainingDaysNames = {"Arms", "Rest", "Chest", "Back", "Shoulders", "Legs", "Neck"};

        printWeekArrayNice (testTrainingSchedule, testTrainingDaysLetters, testTrainingDaysNames, 1);
}

int main ()
{
        //printWeekArrayTest (); PASSED
        //printByLetterTest (); PASSED
        //printWeekArrayNiceTest (); PASSED
}
