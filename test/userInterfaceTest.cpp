#include "../src/userInterface.cpp"

void userInterfaceSplitLengthTest ()
{
        int splitLengthTest = userInterfaceSplitLength ();

        cout << splitLengthTest << '\n';
}

void userInterfaceTrainingDaysNamesTest ()
{
        vector<string> TrainingDaysNamesTest = userInterfaceTrainingDaysNames (7);
        printTrainingDaysNames (TrainingDaysNamesTest);
}

void  userInterfaceTrainingDaysLettersTest ()
{
        vector<char> trainingDaysLettersTest = userInterfaceTrainingDaysLetters (7);
        printTrainingDaysLetters (trainingDaysLettersTest);
}

void userInterfaceStartDayTest ()
{
        int startDayTest = userInterfaceStartDay ();
        cout << startDayTest << '\n';
}

void userInterfaceAConditionTest ()
{
        vector<char> testTrainingDaysLetters = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<string> testTrainingDaysNames = {"Back", "Legs", "Shoulders", "Arms", "Chest", "Rest", "Neck"};
        vector<string> userInterfaceAConditionTestVector = userInterfaceACondition (testTrainingDaysNames, testTrainingDaysLetters);
        printCondition (userInterfaceAConditionTestVector);
}

void userInterfaceNConditionTest ()
{
        vector<char> testTrainingDaysLetters = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<string> testTrainingDaysNames = {"Back", "Legs", "Shoulders", "Arms", "Chest", "Rest", "Neck"};
        vector<string> userInterfaceNConditionTestVector = userInterfaceNCondition (testTrainingDaysNames, testTrainingDaysLetters);
        printCondition (userInterfaceNConditionTestVector);
}

void userInterfaceConditionListTest ()
{
        vector<char> testTrainingDaysLetters = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<string> testTrainingDaysNames = {"Back", "Legs", "Shoulders", "Arms", "Chest", "Rest", "Neck"};
        vector< vector<string> > conditionListTest = userInterfaceConditionList (testTrainingDaysNames, testTrainingDaysLetters);
        printConditionList (conditionListTest);
}

void userInterfaceTest ()
{
        userInterface ();
}

int main ()
{
        //userInterface ();
        //userInterfaceSplitLengthTest (); PASSED
        //userInterfaceTrainingDaysNamesTest (); PASSED
        //userInterfaceTrainingDaysLettersTest (); PASSED
        //userInterfaceStartDayTest (); PASSED
        //userInterfaceAConditionTest (); PASSED
        //userInterfaceNConditionTest (); PASSED
        //userInterfaceConditionListTest (); PASSED
        userInterfaceTest ();
}
