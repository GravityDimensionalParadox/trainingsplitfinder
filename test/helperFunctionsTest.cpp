#include "../src/helperFunctions.cpp"

bool containsLetterSpecificTest ()
{
        vector<char> testTrainingDays = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        bool test1 = containsLetterSpecific ('B', testTrainingDays);
        bool test2 = containsLetterSpecific ('X', testTrainingDays);
        if (test1 == true && test2 == false)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool containsAllLettersTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<char> testTrainingDaysLetters = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        bool calTest = containsAllLetters (testTrainingSchedule, testTrainingDaysLetters);

        vector<char> testTrainingSchedule2 = {'B', 'L', 'X', 'A', 'C', 'R', 'N'};
        bool calTest2 = containsAllLetters (testTrainingSchedule2, testTrainingDaysLetters);

        if (calTest == true && calTest2 == false)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool containsLettersOnlyOnceTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<char> testTrainingDaysLetters = {'A', 'R', 'C', 'B', 'S', 'L', 'N'};
        bool calTest = containsLettersOnlyOnce (testTrainingSchedule, testTrainingDaysLetters);

        vector<char> testTrainingSchedule2 = {'B', 'L', 'S', 'B', 'C', 'R', 'N'};
        bool calTest2 = containsLettersOnlyOnce (testTrainingSchedule2, testTrainingDaysLetters);

        if (calTest == true && calTest2 == false)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool getPositionTest ()
{
        vector<char> testTrainingSchedule = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};

        int test1 = getPosition ('S', testTrainingSchedule);
        int test2 = getPosition ('R', testTrainingSchedule);

        if (test1 == 2 && test2 == 5)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool IntCheckerTest ()
{
        bool test1 = IntChecker ("123456");
        bool test2 = IntChecker ("123A56");

        if (test1 == true && test2 == false)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool StringToIntTest ()
{
        int test1 = StringToInt ("123456");
        int test2 = StringToInt ("123A56");

        if (test1 == 123456 && test2 == 0)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool UserInputXdaysApartIsInNamesVectorTest ()
{
        vector<string> testTrainingDaysNames = {"Arms", "Rest", "Chest", "Back", "Shoulders", "Legs", "Neck"};

        bool test1 = UserInputXdaysApartIsInNamesVector ("Chest", testTrainingDaysNames);
        bool test2 = UserInputXdaysApartIsInNamesVector ("Kek", testTrainingDaysNames);

        if (test1 == true && test2 == false)
        {
                return true;
        }
        else
        {
                return false;
        }
}

bool getPositionTrainingDaysNamesTest ()
{
        vector<string> testTrainingDaysNames = {"Arms", "Rest", "Chest", "Back", "Shoulders", "Legs", "Neck"};

        int test = getPositionTrainingDaysNames ("Back", testTrainingDaysNames);
        if (test == 3)
        {
                return true;
        }
        else
        {
                return false;
        }
}

void printTrainingDaysNamesTest ()
{
        vector<string> TrainingDaysNamesTest = {"Chest", "Arms", "Shoulders", "Legs", "Back", "Rest", "Neck"};
        printTrainingDaysNames (TrainingDaysNamesTest);
}

void printTrainingDaysLettersTest ()
{
        vector<char> trainingDaysLettersTest = {'C', 'A', 'S', 'L', 'B', 'R', 'N'};
        printTrainingDaysLetters (trainingDaysLettersTest);
}

int main ()
{
        //containsLetterSpecificTest (); PASSED
        //containsAllLettersTest (); PASSED
        //containsLettersOnlyOnceTest (); PASSED
        //getPositionTest (): PASSED
        //IntCheckerTest (); PASSED
        //StringToIntTest; PASSED
        //UserInputXdaysApartIsInNamesVectorTest (); PASSED
        //getPositionTrainingDaysNamesTest (); PASSED
        //printTrainingDaysNamesTest (); PASSED
        //printTrainingDaysLettersTest (); PASSED
}
