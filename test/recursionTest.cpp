#include "../src/recursion.cpp"

bool recursiveSolutionTest ()
{
        vector<char> testTrainingDays = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<char> testTrainingSchedule;
        vector<char> testTrainingDaysLetters = {'B', 'L', 'S', 'A', 'C', 'R', 'N'};
        vector<string> testTrainingDaysNames = {"Back", "Legs", "Shoulders", "Arms", "Chest", "Rest", "Neck"};
        int testStartDay = 0;
        vector< vector<string> > testConditionList =
        {
                {"N", "R", "R", "0"},
                {"N", "R", "R", "2"},
                {"N", "R", "R", "3"},
                {"N", "R", "R", "4"},
                {"N", "R", "R", "5"},
                {"N", "R", "R", "6"},

                {"N", "B", "B", "3"},
                {"N", "L", "L", "3"},
                {"N", "N", "N", "0"},
                {"N", "L", "L", "0"},
                {"N", "S", "S", "0"},

                {"A", "B", "A", "1"},
                {"A", "S", "C", "1"},
                {"A", "L", "B", "1"},
                {"A", "S", "A", "1"},
                {"A", "N", "L", "1"}
        };
        recursiveSolution (testTrainingDays, testTrainingSchedule, testTrainingDaysLetters, testTrainingDaysNames, testStartDay, testConditionList);
        return true;
}

int main ()
{
        //recursiveSolutionTest (); PASSED
}
