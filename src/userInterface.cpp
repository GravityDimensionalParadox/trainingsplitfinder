#include "./recursion.cpp"

int userInterfaceSplitLength ()
{

        string userInputSplitLength;
        do
        {
                cout << "how long (days) would you like your split to be?" << '\n';
                cin >> userInputSplitLength;
                if (IntChecker(userInputSplitLength))
                {
                        if (StringToInt (userInputSplitLength) > 52)
                        {
                                cout << "You can't add more than 52 days" << '\n';
                        }
                }
        }
        while (!IntChecker (userInputSplitLength) || !(StringToInt(userInputSplitLength)<=52));

        return StringToInt(userInputSplitLength);
}

vector<string> userInterfaceTrainingDaysNames (int splitLength)
{
        vector<string> trainingDaysNames;
        for (int i = 0; i < splitLength; i++)
        {
                cout << "Enter your " << i+1 << "th traininday" << '\n';
                string userInputTrainingDaysNames;
                cin >> userInputTrainingDaysNames;
                trainingDaysNames.push_back (userInputTrainingDaysNames);
        }
        return trainingDaysNames;
}

vector<char> userInterfaceTrainingDaysLetters (int splitLength)
{
        vector<char> alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        vector<char> trainingDaysLetters;
        for (int i = 0; i < splitLength; i++)
        {
                trainingDaysLetters.push_back(alphabet[i]);
        }
        return trainingDaysLetters;
}

int userInterfaceStartDay ()
{
        string userInputStartDay;
        do
        {
                cout << "Please enter the number for the start day (first day of your training schedule)" << '\n';
                cout << "1) Monday" << '\n';
                cout << "2) Tuesday" << '\n';
                cout << "3) Wednessday" << '\n';
                cout << "4) Thursday" << '\n';
                cout << "5) Friday" << '\n';
                cout << "6) Saturday" << '\n';
                cout << "7) Sunday" << '\n';
                cin >> userInputStartDay;
        }
        while (!IntChecker(userInputStartDay) || StringToInt(userInputStartDay) > 7 || StringToInt(userInputStartDay) < 1);

        return StringToInt(userInputStartDay) - 1;
}

vector<string> userInterfaceACondition (vector<string> trainingDaysNames, vector<char> trainingDaysLetters)
{
        vector<string> emptyCondition;
        emptyCondition.push_back ("A");

        string userInputXdaysApartFirstDay;
        string userInputXdaysApartSecondDay;
        bool inputFirstDayCorrect = false;
        bool inputSecondDayCorrect = false;

        do
        {
                cout << "Enter the first day" << '\n';
                cin >> userInputXdaysApartFirstDay;
                if (UserInputXdaysApartIsInNamesVector(userInputXdaysApartFirstDay, trainingDaysNames))
                {
                        inputFirstDayCorrect = true;

                        do
                        {
                                cout << "Enter the second day" << '\n';
                                cin >> userInputXdaysApartSecondDay;
                                if (UserInputXdaysApartIsInNamesVector(userInputXdaysApartSecondDay, trainingDaysNames))
                                {
                                        inputSecondDayCorrect = true;
                                }
                                else
                                {
                                        cout << "That is not one of the training days you entered" << '\n';
                                }
                        }
                        while (!inputSecondDayCorrect);
                }
                else
                {
                        cout << "That is not one of the training days you entered" << '\n';
                }
        }
        while(!inputFirstDayCorrect);

        int posOfFirstDay =  getPositionTrainingDaysNames (userInputXdaysApartFirstDay, trainingDaysNames);
        int posOfSecondDay = getPositionTrainingDaysNames (userInputXdaysApartSecondDay, trainingDaysNames);

        string charFirstDay = "X";
        string charSecondDay = "X";

        charFirstDay[0] = trainingDaysLetters[posOfFirstDay];
        charSecondDay[0] = trainingDaysLetters[posOfSecondDay];

        emptyCondition.push_back(charFirstDay);
        emptyCondition.push_back(charSecondDay);

        string userInputIntDaysApart;
        do
        {
                cout << "Enter how many days you want these training days apart" << '\n';
                cout << "Mo Tu We Th Fr Sa Su" << '\n';
                cout << "Ch    Le" << '\n';
                cout << "These trainingdays are 1 day apart \n";
                cin >> userInputIntDaysApart;
                if (!IntChecker(userInputIntDaysApart))
                {
                        cout << "That's not an integer" << '\n';
                }
        }
        while(!IntChecker(userInputIntDaysApart));

        emptyCondition.push_back (userInputIntDaysApart);

        return emptyCondition;
}

vector<string> userInterfaceNCondition (vector<string> trainingDaysNames, vector<char> trainingDaysLetters)
{
        vector<string> emptyCondition;

        emptyCondition.push_back ("N");

        string userInputNotOnDayXfirstDay;
        bool inputNotOnDayCorrect = false;
        do
        {
                cout << "Enter training day" << '\n';
                cin >> userInputNotOnDayXfirstDay;
                if (UserInputXdaysApartIsInNamesVector(userInputNotOnDayXfirstDay, trainingDaysNames))
                {
                        inputNotOnDayCorrect = true;
                }
                else
                {
                        cout << "That is not one of the training days you entered" << '\n';
                }
        }
        while (!inputNotOnDayCorrect);

        int posOfNotOnDay =  getPositionTrainingDaysNames (userInputNotOnDayXfirstDay, trainingDaysNames);
        char charFirstDay = trainingDaysLetters[posOfNotOnDay];

        string stringFirstDay = "X";
        stringFirstDay[0] = charFirstDay;

        emptyCondition.push_back(stringFirstDay);
        emptyCondition.push_back(stringFirstDay);

        string userInputNotOnDay;
        do
        {
                cout << "Please enter the number of the day you don't want the training to fall on" << '\n';
                cout << "1) Monday" << '\n';
                cout << "2) Tuesday" << '\n';
                cout << "3) Wednessday" << '\n';
                cout << "4) Thursday" << '\n';
                cout << "5) Friday" << '\n';
                cout << "6) Saturday" << '\n';
                cout << "7) Sunday" << '\n';
                cin >> userInputNotOnDay;
        }
        while (!IntChecker(userInputNotOnDay) || StringToInt(userInputNotOnDay) > 7 || StringToInt(userInputNotOnDay) < 1);

        int intNotOnDay = StringToInt(userInputNotOnDay)-1;

        emptyCondition.push_back (to_string(intNotOnDay));

        return emptyCondition;
}

vector< vector<string> > userInterfaceConditionList (vector<string> trainingDaysNames, vector<char> trainingDaysLetters)
{

        //soortCondition:
        //              atleastXdaysApart:      A
        //              notOnDayX:              N
        //              0                    1                        2              3
        //format: [char soortCondition][char FirstDay][char (possible)secondDay][int amountDays/weekDay]

        string userInputCondition;

        vector< vector<string> > conditionList;
        do
        {
                cout << "You can now enter your requirements, enter the letter of what you want to do" << '\n';
                cout << "A) Atleast X days apart, for if you want to have 2 particular atleast X days apart" << '\n';
                cout << "N) Not On Day X, for you dont want a particular training on a particular day" << '\n';
                cout << "Q) Quit, calculates the schedules" << '\n';

                cin >> userInputCondition;

                if (userInputCondition == "A" || userInputCondition == "a")
                {
                        vector<string> emptyCondition = userInterfaceACondition (trainingDaysNames, trainingDaysLetters);

                        conditionList.push_back (emptyCondition);
                }
                else if (userInputCondition == "N" || userInputCondition == "n")
                {
                        vector<string> emptyCondition = userInterfaceNCondition (trainingDaysNames, trainingDaysLetters);

                        conditionList.push_back (emptyCondition);
                }
                else if (userInputCondition == "Q" || userInputCondition == "q")
                {
                        //do nothing
                }
                else
                {
                        cout << "That is not one of the options" << '\n';
                }
        }
        while (!(userInputCondition == "Q" || userInputCondition == "q"));

        return conditionList;
}

void userInterface ()
{

        int splitLength = userInterfaceSplitLength ();

        vector<string> trainingDaysNames = userInterfaceTrainingDaysNames (splitLength);

        vector<char> trainingDaysLetters = userInterfaceTrainingDaysLetters (splitLength);

        vector<char> trainingDays = trainingDaysLetters;

        vector<char> trainingSchedule;

        int startDay = userInterfaceStartDay ();

        vector< vector<string> > conditionList = userInterfaceConditionList (trainingDaysNames, trainingDaysLetters);

        //(vector<char> trainingDays, vector<char> trainingSchedule, vector<char> trainingDaysLetters, vector<string> trainingDaysNames, int startDay, vector< vector<string> > conditionList;
        recursiveSolution (trainingDays, trainingSchedule, trainingDaysLetters, trainingDaysNames, startDay, conditionList);
}
