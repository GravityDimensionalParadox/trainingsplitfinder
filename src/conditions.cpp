#include "./helperFunctions.cpp"

//Returns if the first specified day of training and the second specified day of training are at least X days apart in the trainingSchedule (both ways)
bool atleastXdaysApart (char firstDay, char secondDay, int days, vector<char> trainingSchedule)
{
        if (containsLetterSpecific (firstDay, trainingSchedule) && containsLetterSpecific (secondDay, trainingSchedule))
        {
                int posFD = getPosition (firstDay, trainingSchedule);
                int posSD = getPosition (secondDay, trainingSchedule);

                int firstDayInWeek;
                int secondDayInWeek;

                if (posFD < posSD)
                {
                        firstDayInWeek = posFD;
                        secondDayInWeek = posSD;
                }
                else
                {
                        firstDayInWeek = posSD;
                        secondDayInWeek = posFD;
                }

                if (secondDayInWeek - firstDayInWeek - 1 >= days)
                {
                        if (firstDayInWeek - 1 + (6 - secondDayInWeek) >= days)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
                else
                {
                        return false;
                }
        }
        else
        {
                return true;
        }
}

//Returns if a certain trainingDay doesn't fall on a certain week day, returns true if the trainingDay doesnt fall on the day, and false if it does
//with for weekDay Mo = 0 and Su=6
bool notOnDayX (char trainingDay, int weekDay, vector<char> trainingSchedule, int startDay)
{
        if (containsLetterSpecific(trainingDay, trainingSchedule))
        {
                int pos = getPosition (trainingDay, trainingSchedule);
                if (pos == (weekDay+startDay)%7)
                {
                        return false;
                }
                else
                {
                        return true;
                }
        }
        else
        {
                return true;
        }
}

//A function that checks all conditions in the conditionList, and returns true if they all return true, and otherwise returns false
bool findConditions (vector<char> trainingSchedule, vector< vector<string> > conditionList, int startDay)
{
        //soortCondition:
        //              atleastXdaysApart:      A
        //              notOnDayX:              N
        //              0                    1                        2              3
        //format: [char soortCondition][char FirstDay][char (possible)secondDay][int amountDays/weekDay]

        for(int i = 0; i < conditionList.size (); i++)
        {
                if (conditionList[i][0] == "A")
                {
                        if (!atleastXdaysApart (conditionList[i][1][0], conditionList[i][2][0], StringToInt(conditionList[i][3]), trainingSchedule))
                        {
                                return false;
                        }
                }
                else
                {
                        if (!notOnDayX (conditionList[i][1][0], StringToInt(conditionList[i][3]), trainingSchedule, startDay))
                        {
                                return false;
                        }
                }
        }
        return true;
}
