#include "./printFunctions.cpp"
#include <math.h>

//Returns if the trainingSchedule contains a specified letter
bool containsLetterSpecific (char day, vector<char> trainingSchedule)
{
        for (int i = 0; i < trainingSchedule.size (); i++)
        {
                if (trainingSchedule[i] == day)
                {
                        return true;
                }
        }
        return false;
}

//Returns if a trainingSchedule contains all the specified trainingDaysLetters
bool containsAllLetters (vector<char> trainingSchedule, vector<char> trainingDaysLetters)
{
        for (int i = 0; i < trainingDaysLetters.size (); i++)
        {
                if (!containsLetterSpecific (trainingDaysLetters[i], trainingSchedule))
                {
                        return false;
                }
        }
        return true;
}

//Returns if a trainingSchedule contains all op the specified letters only once
bool containsLettersOnlyOnce (vector<char> trainingSchedule, vector<char> trainingDaysLetters)
{
        for (int i = 0; i < trainingDaysLetters.size (); i++)
        {
                char letterToCheck = trainingDaysLetters[i];
                int letterCount = 0;
                for (int j = 0; j < trainingSchedule.size (); j++)
                {
                        if (letterToCheck == trainingSchedule[j])
                        {
                                letterCount++;
                        }
                }
                if (letterCount > 1)
                {
                        return false;
                }
        }
        return true;
}

//Returns the position of a certain trainingDay in a trainingSchedule
int getPosition (char day, vector<char> trainingSchedule)
{
        for (int i = 0; i < trainingSchedule.size (); i++)
        {
                if (day == trainingSchedule[i])
                {
                        return i;
                }
        }
        return -1;
}

bool IntChecker (string checkString)
{
        for(int i=0; i<checkString.size(); i++)                                 //Loops through the string
        {
                if(checkString[i] >= '0' && checkString[i] <= '9')              //Checks if all chars are a number
                {
                }
                else                                                            //If not:
                {
                        return false;                                           //return false
                }
        }
        return true;                                                            //If looped through all numbers succesfully: return true
}

int StringToInt (string numberString)
{
        if(IntChecker(numberString))                                            //Checks if the string contains only numbers
        {
                int transferInt = 0;                                            //An int that is used to add up all the numbers in the string into
                for(int i = numberString.size () - 1; i >= 0 ; i--)             //Loop through all the numbers in the string, starting from the most right one and moving left
                {
                        int positionInt;                                        //An int that holds the value of the number that is at the i'th place in the string
                        switch (numberString[i])                                //Change the value of positionInt according to the number at the i'th place
                        {                                                       //
                                case '1': positionInt = 1; break;               //Change the valua of positionInt to 1 if numberString[i] = '1'
                                case '2': positionInt = 2; break;               //Change the valua of positionInt to 2 if numberString[i] = '2'
                                case '3': positionInt = 3; break;               //Change the valua of positionInt to 3 if numberString[i] = '3'
                                case '4': positionInt = 4; break;               //Change the valua of positionInt to 4 if numberString[i] = '4'
                                case '5': positionInt = 5; break;               //Change the valua of positionInt to 5 if numberString[i] = '5'
                                case '6': positionInt = 6; break;               //Change the valua of positionInt to 6 if numberString[i] = '6'
                                case '7': positionInt = 7; break;               //Change the valua of positionInt to 7 if numberString[i] = '7'
                                case '8': positionInt = 8; break;               //Change the valua of positionInt to 8 if numberString[i] = '8'
                                case '9': positionInt = 9; break;               //Change the valua of positionInt to 9 if numberString[i] = '9'
                                case '0': positionInt = 0; break;               //Change the valua of positionInt to 0 if numberString[i] = '0'
                        }
                        int whereInt = numberString.size()-i;                   //An integer that holds the place of where the cursor currently is in the string, but counted from the right, so the most right number will give an whereInt of 1
                        transferInt = transferInt + positionInt*pow(10, whereInt); //Set the transferInt to itself + the current positionInt times 10 to the power of the whereInt
                }
                transferInt = transferInt/10;                                   //Divide the transferInt by 10
                return transferInt;                                             //return the transferInt, a string like "123" will return 123
        }
        else                                                                    //If the string doesn't contain only numbers, return 0
        {
                return 0;
        }
}

bool UserInputXdaysApartIsInNamesVector (string userInput, vector<string> trainingDaysNames)
{
        for (int i = 0; i < trainingDaysNames.size (); i++)
        {
                if (userInput == trainingDaysNames[i])
                {
                        return true;
                }
        }
        return false;
}


int getPositionTrainingDaysNames (string trainingDay, vector<string> trainingDaysNames)
{
        for (int i = 0; trainingDaysNames.size (); i++)
        {
                if (trainingDay == trainingDaysNames[i])
                {
                        return i;
                }
        }
        return -1;
}

void printTrainingDaysNames (vector<string> trainingDaysNames)
{
        for (int i = 0; i < trainingDaysNames.size (); i++)
        {
                cout << i + 1 << ") " << trainingDaysNames[i] << '\n';
        }
        cout << '\n';
}

void printTrainingDaysLetters (vector<char> trainingDaysLetters)
{
        for (int i = 0; i < trainingDaysLetters.size (); i++)
        {
                cout << trainingDaysLetters[i] << '\n';
        }
        cout << '\n';
}

void printCondition (vector<string> condition)
{
        for (int i = 0; i < condition.size (); i++)
        {
                cout << i << ") " << condition[i] << '\n';
        }
        cout << '\n';
}

void printConditionList (vector< vector<string> > conditionList)
{
        for (int i = 0; i < conditionList.size (); i++)
        {
                printCondition (conditionList[i]);
        }
        cout << '\n';
}
