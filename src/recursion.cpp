#include "./conditions.cpp"

void recursiveSolution (vector<char> trainingDays, vector<char> trainingSchedule, vector<char> trainingDaysLetters, vector<string> trainingDaysNames, int startDay, vector< vector<string> > conditionList)
{
        if(trainingDays.size () > 0)
        {
                for(int i = 0; i < trainingDays.size(); i++)
                {
                        vector<char> trainingDaysCopy = trainingDays;
                        vector<char> trainingScheduleCopy = trainingSchedule;

                        trainingScheduleCopy.push_back(trainingDaysCopy[i]);
                        trainingDaysCopy.erase(trainingDaysCopy.begin()+i);

                        if(findConditions (trainingScheduleCopy, conditionList, startDay))
                        {
                                recursiveSolution (trainingDaysCopy, trainingScheduleCopy, trainingDaysLetters, trainingDaysNames, startDay, conditionList);
                        }
                        else
                        {
                                //Discard
                        }

                }
        }
        else
        {
                if (containsAllLetters (trainingSchedule, trainingDaysLetters) && containsLettersOnlyOnce (trainingSchedule, trainingDaysLetters))
                {
                        printWeekArrayNice(trainingSchedule, trainingDaysLetters, trainingDaysNames, startDay);
                }
                else
                {
                        //Discard
                }
        }
}
