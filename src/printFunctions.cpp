#include <vector>
#include <string>
#include <iostream>

using namespace std;

//Prints out a trainingSchedule by letter
void printWeekArray (vector<char> trainingSchedule)
{
        for (int i = 0; i < trainingSchedule.size (); i++)
        {
                cout << trainingSchedule[i];
        }
        cout << '\n';
}

//Prints out the corresponding trainingName for a specified letter
string printByLetter (char theLetter, vector<char> trainingDaysLetters, vector<string> trainingDaysNames)
{
        for (int i = 0; i < trainingDaysLetters.size(); i++)
        {
                if (theLetter == trainingDaysLetters[i])
                {
                        return trainingDaysNames[i];
                }
        }
        return "NotFound";
}

//Prints out the corresponding trainingDaysNames for an entire trainingSchedule
void printWeekArrayNice (vector<char> trainingSchedule, vector<char> trainingDaysLetters, vector<string> trainingDaysNames, int startDay)
{
        vector<string> daysOfWeek = {"Monday: ", "Tuesday: ", "Wednessday: ", "Thursday: ", "Friday: ", "Saturday: ", "Sunday: "};

        for(int i = 0; i < trainingSchedule.size (); i++)
        {
                cout << daysOfWeek[(i+startDay)%7] << printByLetter (trainingSchedule[i], trainingDaysLetters, trainingDaysNames) << '\n';
        }
        cout << '\n';
}
